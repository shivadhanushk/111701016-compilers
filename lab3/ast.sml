

structure Ast = struct

datatype Program = Expr of Exp |
		Declare of Dec list

and Binop = Plus | Minus | Mul | Div | Lthan | Lthaneq | Eq | Noteq | Gthan |Gthaneq | Or | And
and Exp =  Nil |
	  Const of int 
          | Line of string 
	  | New of string
          | Fun_call of (string * Exp list)
 	  | Met_call of (string*string*Exp list)
	  | Op of (Exp * Binop * Exp)
	  | Exps of Exp list
	  | IF of Exp*Exp
	  | Else of Exp * Exp * Exp
	  | WHILE of Exp*Exp
	  | FOR of string*Exp*Exp*Exp
	  | LET of Dec list * Exp list
	  | Break
	  | Createarray1 of string * Exp * Exp
	  | Assign of Lvalue*Exp
 	  | Createrecord of string * (string * Exp)list
                            (*type_id *) (*(ID * Exp)list*)
	  | Lval of Lvalue


and Lvalue = Var of string (*ID*)
	|  Lvalue1 of(Lvalue* string)
	| Lvalue2 of (Lvalue*Exp)

and Dec = Typedec of string*string
	|Var_dec1 of string*string*Exp
	|Var_dec2 of string*Exp
	|Fun_dec1 of string * (string * string) list * Exp

		    (*id   *  (id * type_id) list    * type_id * Exp*)
	| Fun_dec2 of string * (string * string) list * string * Exp
	| Prim_dec1 of string* (string * string)list
	| Prim_dec2 of string*string*(string*string)list
	| Import of  string
	| Array_dec of string*string
	| Rec_dec   of string * (string*string) list

(* Suggestion to use the record syntax of SML

   https://www.cs.cornell.edu/courses/cs312/2008sp/recitations/rec02.html

*)



end (* structure Tiger *)

