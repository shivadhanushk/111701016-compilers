structure Ast_print = 
struct 
fun printwhole (Ast.Expr e) = String.concat ["Exp(",exp(e),")\n"]
	 | printwhole (Ast.Declare d) = String.concat["Declare(",decs(d),")\n"]  


and 

 opp Ast.Plus = "Plus"
	| opp Ast.Minus = "Minus"
	| opp Ast.Div = "Div"
	| opp Ast.Mul = "Mul"
	| opp Ast.Lthan = "Lthan"
	| opp Ast.Lthaneq = "Lthaneq"
	| opp Ast.Eq = "Equal"
	| opp Ast.Noteq = "Noteq"
	| opp Ast.Gthan = "Gthan"
	| opp Ast.Gthaneq = "Gthaneq"
	| opp Ast.Or = "Or"
	| opp Ast.And = "And"
and
exp(Ast.Nil) = "Nil"
	|exp(Ast.Const(a)) = Int.toString a 
	| exp (Ast.Line(a))= "String(" ^ a ^ ")\n" 
 	|exp(Ast.IF(a,b)) = "If("^ exp a ^ "," ^"Then \n"^ exp b ^ ")\n"
	|exp(Ast.Else(a,b,c)) = "If_else(" ^ exp a ^ ",\n" ^ exp b ^ ",\n" ^ exp c  ^ ")\n"
	|exp(Ast.FOR(a,b,c,d)) = "For(" ^ a ^",\n" ^ exp b ^ "\n" ^ exp c ^ "\n" ^ exp d ^")\n"
	|exp(Ast.WHILE(a,b)) =  "While(" ^ exp a  ^ ",\n" ^ "Do\n" ^ exp b ^ ")\n"
        |exp(Ast.LET(a,b)) =     "Let(" ^ decs a ^ ",\n" ^ explist b ^ ")\n" 
	| exp(Ast.Break) = "Break" ^ "\n"
	| exp(Ast.Createarray1(a,b,c)) =  "Array("^ a ^ exp b ^ ",\n" ^ exp c ^ ")\n"
        |exp(Ast.Createrecord(a,b)) = "record("^ a ^ "\n" ^ reclist  b ^ ")\n" 
	|exp(Ast.Op(a,b,c))= "Binop(" ^ exp a  ^ ",\n" ^ opp b ^ ",\n" ^ exp c ^")\n"
	|exp (Ast.Fun_call(a,blist)) = "Function_call(" ^ a ^",\n"^ explist blist ^ ")\n"
	|exp(Ast.Met_call(a,b,clist)) = "Method_call(" ^ a ^ ",\n" ^ b ^ "\n" ^ explist clist ^ ")\n"
	| exp (Ast.Exps elist) = explist elist ^ "\n"
	| exp (Ast.Assign(a,b) ) =  "Assign(" ^ lvalue a ^ ",\n" ^ exp b ^ ")\n"
	| exp(Ast.Lval(a)) = lvalue( a )
	| exp(Ast.New(a)) = "New("^ a ^")\n"
	
	


and explist a = String.concat(List.map(fn b  => exp b ^ ", ") a)
and reclist a = String.concat(List.map(fn(b,c)   => b ^ ", " ^ exp c ^ ",\n" ) a)
and str_tuplist a = String.concat(List.map(fn(b,c) => b ^ "," ^ c ^ ",\n" ) a) 
and 
 dec (Ast.Typedec(a,b))= "Typedeclare(" ^ a ^ ",\n" ^ b ^  ")\n" 
     |dec (Ast.Var_dec1(a,b,c))="Vardeclare(" ^ a ^ ",\n" ^ b ^ ",\n" ^ exp c ^ ")\n"
     | dec (Ast.Var_dec2(a,b)) = "Vardeclare(" ^ a ^ ",\n" ^ exp b ^ ")\n"
     | dec(Ast.Fun_dec1(a,slist, e))= "Functiondeclare(" ^ a ^ ",\n" ^ str_tuplist slist ^ ",\n" ^ exp e 
     | dec(Ast.Fun_dec2(a,slist, b , e))= "Functiondeclare(" ^ a ^ ",\n" ^ str_tuplist slist ^ ",\n" ^ b ^ ",\n" ^ exp e ^ ")\n" 
     | dec(Ast.Prim_dec1(a,blist))= "Primitive("^a^str_tuplist blist^ ")\n"
     | dec(Ast.Prim_dec2(a,b,clist))= "Primitive("^ a ^ b ^ str_tuplist clist ^ ")\n"
     | dec(Ast.Import(a))= "import("^ a ^")\n"  
     | dec(Ast.Array_dec  (a,b))=  "Arraydeclare(" ^ a ^ ",\n" ^ b ^ ")\n"
     | dec(Ast.Rec_dec  (a,slist))= "Recorddeclare(" ^ a ^ ",\n" ^ str_tuplist slist ^ ")\n"
 	

and
decs d = String.concat(List.map  (fn a => dec a ^ ",\n ") d)

and
lvalue (Ast.Lvalue1(a,b))=  "LVALUE1(" ^ lvalue a ^ ",\n" ^ b ^       ")\n"
	| lvalue (Ast.Lvalue2(a,b))=   "LVALUE2(" ^ lvalue a ^ ",\n" ^ exp b ^      ")\n"
	| lvalue (Ast.Var(a)) =  "Var(" ^ a ^ ")\n"

end

