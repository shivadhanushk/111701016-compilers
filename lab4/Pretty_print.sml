structure Pretty_print = 
struct

fun prettyprint(Ast.Expr e) =  exp(e) 
	| prettyprint(Ast.Declare d) = decs(d)

and 

    spacer(0) = ""
	| spacer(x) = " " ^ spacer(x-1)

and 
 opp Ast.Plus = "\027[35m+"
	| opp Ast.Minus = "\027[35m-"
	| opp Ast.Div = "\027[35m/"
	| opp Ast.Mul = "\027[35m*"
	| opp Ast.Lthan = "\027[35m<"
	| opp Ast.Lthaneq = "\027[35m<="
	| opp Ast.Eq = "\027[35m="
	| opp Ast.Noteq = "\027[35m!="
	| opp Ast.Gthan = "\027[35m>"
	| opp Ast.Gthaneq = "\027[35m>="
	| opp Ast.Or = "\027[35m|"
	| opp Ast.And = "\027[35m&"
and
exp(Ast.Nil) = "Nil"
	|exp(Ast.Const(a)) =  "\027[33m" ^ Int.toString a 
	| exp (Ast.Line(a))=  id a 
 	|exp(Ast.IF(a,b)) = key "if "^ exp a ^ " " ^ key "\n then \n"^ " " ^ exp b ^ "\n"
	|exp(Ast.Else(a,b,c)) = key "if " ^ exp a ^ " " ^ key "\n then \n " ^ exp b ^ key " \n else\n" ^ exp c  ^ "\n"
	|exp(Ast.FOR(a,b,c,d)) = key "for " ^ id a ^" := " ^ exp b ^ key "to " ^ exp c ^ key "\n do \n " ^ exp d ^"\n"
	|exp(Ast.WHILE(a,b)) = key  " while " ^ exp a  ^ key "\n do\n "  ^ exp b ^ "\n"
        |exp(Ast.LET(a,b)) =     key " Let\n" ^ decs a ^ "\n" ^ key " in " ^"\n" ^ explist b ^ "\n" ^ key "end" ^ "\n" 
	| exp(Ast.Break) = key "Break " ^ "\n"
	| exp(Ast.Createarray1(a,b,c)) =  id a ^ " \027[35m[" ^ exp b ^ " \027[35m]" ^ key " of " ^ exp c ^ "\n"
        |exp(Ast.Createrecord(a,b)) = id a ^ " \027[35m{ " ^ reclist  b ^ " \027[35m } " ^")\n"
 
	|exp(Ast.Op(a,b,c))=   exp a  ^ " " ^ opp b ^ " " ^ exp c 
	|exp (Ast.Fun_call(a,blist)) =  id a ^"\027[35m( "^ explist blist ^ "\027[35m)\n"
	|exp(Ast.Met_call(a,b,clist)) = id a ^ "(" ^  b  ^ explist clist ^ ")\n"
	| exp (Ast.Exps elist) = "\027[35m( " ^ explist elist ^ " \027[35m)\n"
	| exp (Ast.Assign(a,b) ) =    lvalue a ^ " := " ^ exp b 
	| exp(Ast.New(a)) = key "New" ^ id a
	| exp(Ast.Lval(a)) = lvalue( a )
	
	
	
and id s = "\027[32m" ^ s ^ " " 
and key s = "\027[31m" ^ s ^ " "




and explist a = String.concat(List.map(fn b  => exp b ^ "\027[35m;") a)
and reclist a = String.concat(List.map(fn(b,c)   => id b ^ "\027[35m:" ^ exp c ^ ", \n" ) a)
and str_tuplist a = String.concat(List.map(fn(b,c) => id b ^ "\027[35m:" ^ id c ^ ", " ) a) 
and 
 dec (Ast.Typedec(a,b))= key " type " ^ id a ^ " \027[35m= "  ^ id b  
     |dec (Ast.Var_dec1(a,b,c))= key " Var " ^ id a ^ "\027[35m:" ^ b ^ "\027[35m=" ^ exp c ^ " " 
     | dec (Ast.Var_dec2(a,b)) = key " Var " ^ id a ^ " = " ^ exp b ^" "
     | dec(Ast.Fun_dec1(a,slist, e))= key "function" ^ id a ^ "\027[35m(" ^  str_tuplist slist ^ "\027[35m)\n" ^ " = "^ exp e 
     | dec(Ast.Fun_dec2(a,slist, b , e))= key "function" ^ id a ^"\027[35m(" ^  str_tuplist slist ^ "\027[35m) :" ^ id b^ " \027[35m\n ="  ^ exp e  ^ "\n"
     | dec(Ast.Prim_dec1(a,blist))= "Primitive"^ id a ^ "("^str_tuplist blist^ ")\n"
     | dec(Ast.Prim_dec2(a,b,clist))= "Primitive"^ id a ^"("^ b ^","^ str_tuplist clist ^ ")\n"   
     | dec(Ast.Import(a)) = "import" ^ id a 
     | dec(Ast.Array_dec  (a,b))=  key "type" ^ id a ^ "\027[35m = " ^ key " array of "^ id b ^ " "
     | dec(Ast.Rec_dec  (a,slist))= key "type" ^ id a ^ "\027[35m = " ^ "\027[35m{ " ^str_tuplist slist ^"\027[35m}" ^ " "
 

and
decs d = String.concat(List.map  (fn a => dec a ^ "\n") d)

and
lvalue (Ast.Lvalue1(a,b))=   lvalue a ^ "\027[35m." ^ id b 
	| lvalue (Ast.Lvalue2(a,b))=     lvalue a ^ "\027[35m[" ^ exp b ^ "\027[35m]" 
	| lvalue (Ast.Var(a)) =  id a

end

